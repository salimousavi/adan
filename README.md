# AdanVideos

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>adan</code>.<br>

<b>Fields:</b>
 <br>
 <code> field_adan_type </code>
 <code> field_adan_video </code>

<b>Route request:</b>
 <br>
 <code> api/adan/videos </code>

<b>Response:</b>
 <br><code> name </code> Example: اذان صبح 
 <br><code> type </code> Example: fajr, dhuhr, maghrib
 <br><code> src </code> Video source
