<?php

namespace Drupal\adan\Controller;

use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class AdanController {

    public function index() {

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'adan']);

        $result = [];
        foreach ($terms as $term) {

            $result[] = [
                'name' => $term->getName(),
                'type' => $term->get('field_adan_type')->getValue()[0]['value'],
                'src' => $term->get('field_adan_video')->isEmpty() ? null : $term->field_adan_video->entity->uri->value,
            ];
        }

        return new JsonResponse($result);
    }

}